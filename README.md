# My project's README

The main activity in the app displays a list of songs from various artists. When the user clicks on a list item,
the main activity is stopped, and a browser window is displayed instead. The browser window shows a page
with a publicly available video segment playing the song. By clicking a play button on the page, the device
user can watch the video and listen to the song. A user would return to the main activity by pressing the
device’s “back” soft key when the video clip is complete.
In addition, each list item should support “long click” functionality. A long click on any list item will
bring up a “context menu” showing the following three options for the song under consideration: (1) View
the video clip (similar to a simple click); (2) View a browser’s page with the song’s wikipedia page; and (3)
View a browser’s page with the artist (or band) wikipedia page